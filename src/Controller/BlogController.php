<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Form\ArticleType;



class BlogController extends AbstractController {


    /**
     * @Route("/", name="home_page")
     */

    public function homePage(ArticleRepository $repo){
        return $this->render("home.html.twig", [
            "onearticle" => $repo->findLastByDate()
        ]);
    }

    /**
     * @Route("/articles", name="articles")
     */

     public function articlePage(ArticleRepository $repo){
         return $this->render("articles.html.twig",[
            'articles' => $repo->findAll()
         ]);
     }

     /**
      * @Route("/ajout-article", name="ajout_article")
      */

     public function addArticle(Request $request, ArticleRepository $repo){

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $repo->add($article);
            dump($article);
            return $this->redirectToRoute('articles');
            
        }

        
        return $this->render("add-article.html.twig",[
            "form" => $form->createView(),
            "article" => $article

        ]);
     }

     /**
     * @Route("/article/{id}", name="one_article")
     */

    public function oneArticle(int $id, ArticleRepository $repo){

        return $this->render("one-article.html.twig",[
            "onearticle" => $repo->find($id)
        ]);
     }
    
    /**
     * @Route("delete/article/{id}", name="delete_article")
     */

    public function remove(int $id, ArticleRepository $repo){
        $repo->remove( $repo->find($id));

        return $this->redirectToRoute('articles');
    }
}