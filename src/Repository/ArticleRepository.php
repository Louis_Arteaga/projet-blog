<?php

namespace App\Repository;

use App\Entity\Article;

 
class ArticleRepository {

    public function findAll():array {
        $articles = [];

        $connection = Connexion::getConnection();
        
        $query = $connection->prepare("SELECT * FROM article");
       
        $query->execute();
       
        $results = $query->fetchAll();
        foreach ($results as $line) {
            $articles[] = $this->sqlToArticle($line);
        }
        
        return $articles;
    }


    public function add(Article $article): void {
        //On récupère notre connexion PDO avec notre classe utilitaire
        $connection = Connexion::getConnection();
        
        $query = $connection->prepare("INSERT INTO article (titre,date,contenu,auteur) VALUES (:titre,NOW(),:contenu,:auteur)");
        
        $query->bindValue(":titre", $article->titre, \PDO::PARAM_STR);
//        $query->bindValue(":date", $article->date, \PDO::PARAM_);
        $query->bindValue(":contenu", $article->contenu, \PDO::PARAM_STR);
        $query->bindValue(":auteur", $article->auteur, \PDO::PARAM_STR);
        $query->execute();

        
        $article->id = $connection->lastInsertId();
    }

    private function sqlToArticle(array $line): Article {
        
        $article = new Article();
        
        $article->id = intval($line["id"]);
        $article->titre = $line["titre"];
        $article->date = $line["date"];
        $article->contenu = ($line["contenu"]);
        $article->auteur = ($line["auteur"]);
        
        return $article;
    }

    public function find(int $id): ?Article {
        $connection = Connexion::getConnection();
        
        $query = $connection->prepare("SELECT * FROM article WHERE id=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        
        if($line = $query->fetch()) {
            
            return $this->sqlToArticle($line);

        }
       
        return null;
    }

    public function findLastByDate(): ?Article {
        $connection = Connexion::getConnection();
        
        $query = $connection->prepare("SELECT * FROM article WHERE article.date = (SELECT MAX(article.date) FROM article)");
        $query->execute();
        
        if($line = $query->fetch()) {
            
            return $this->sqlToArticle($line);
        }
       
        return null;
    }

    public function remove(Article $article): void {
        $connection = Connexion::getConnection();

        $query = $connection->prepare("DELETE FROM article WHERE id=:id");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();
    }




}